## Feature Request

### Summary
<!-- Provide a concise summary of the feature request -->

### Problem to solve
<!-- Describe the problem or need that this feature request addresses -->

### Proposal
<!-- Provide a detailed description of the proposed feature -->

### Links / references
<!-- Add any relevant links, such as related issues, forum discussions, or external resources -->

### Additional information
<!-- Add any other context or information that might be relevant for understanding the feature request -->