## Bug Report

### Summary
<!-- Provide a concise summary of the bug -->

### Steps to Reproduce
1. <!-- First step to reproduce the bug -->
2. <!-- Second step to reproduce the bug -->
3. <!-- Additional steps as necessary -->

### Example Project
<!-- If applicable, provide a link to a public project or a specific commit where the bug can be reproduced -->

### What is the current bug behavior?
<!-- Describe the current behavior of the application that indicates a bug -->

### What is the expected correct behavior?
<!-- Describe what the correct behavior should be -->

### Relevant logs and/or screenshots
<!-- Include any relevant logs or screenshots that may help to diagnose the issue -->

### Possible fixes
<!-- If you can, provide any suggestions on how this bug could be fixed -->

### Environment
<!-- Provide as much detail as possible about your environment, such as: -->
- Operating System:
- Any other relevant information:

### Additional information
<!-- Add any other context about the problem here. -->