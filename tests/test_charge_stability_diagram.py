"""
End to end test for the simulaton of a charge stability diagrams
using a double quantum dot device.
"""

import pytest
from qdsim import QDDevice
from qdsim import QDSimulator
import os


@pytest.fixture
def qddevice():
    qddevice = QDDevice()
    qddevice.one_dimensional_dots_array(n_dots=2)
    return qddevice


@pytest.fixture
def qdsimulator_electrons():
    qdsimulator = QDSimulator('Electrons')
    return qdsimulator


class TestSimulationStabilityDiagram:
    """
    End to end test associated with Tutotial 1, steps 1 to 3. Simulating a charge stability diagrams
    for a double quantum dot.
    """

    def test_simulate_charge_stability_diagram(self, qdsimulator_electrons, qddevice):

        # run simulation
        qdsimulator_electrons.set_sensor_locations([(2, 1)])
        qdsimulator_electrons.simulate_charge_stability_diagram(
            qd_device=qddevice,
            scanning_gate_indexes=[0, 1],
            n_points_per_axis=60,
            v_range_x=[-5, 20],
            v_range_y=[-5, 20],
            solver='MOSEK',
        )

        # Checks probe_voltage_space produces output
        assert qdsimulator_electrons._occupation_array is not None
        assert qdsimulator_electrons._energy_array is not None
        assert qdsimulator_electrons._time_list is not None

        # Checks voltage_occupation_data produces data during simulation
        assert qdsimulator_electrons._voltage_occupation_data is not None

        # Checks system_maxwell_matrix contains data
        assert qdsimulator_electrons._system_maxwell_matrix is not None

        # Plot charge stability diagram
        TMP_DIR = 'tests/tmp'
        try:
            os.makedirs(TMP_DIR, exist_ok=True)
            qdsimulator_electrons.plot_charge_stability_diagrams(cmapvalue='RdPu_r', 
                                                            save_plot_to_filepath=f'{TMP_DIR}/DQD_current.png')
        except Exception as e:
            print(e)
            # Test will fail if the plot is not produced for any reason. 
            assert False, "Plotting failed."
        else:
            # Check if the plot was saved to file in the intended location
            assert os.path.exists(f'{TMP_DIR}/DQD_current.png'), "Plot was not saved to file."
        finally:
            # Clean up
            os.system(f'rm -rf {TMP_DIR}')

