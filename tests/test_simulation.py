"""
Unit tests for the qddevice module.
"""

import pytest
from qdsim import QDDevice


@pytest.fixture
def qddevice():
    qddevice = QDDevice()
    qddevice.one_dimensional_dots_array(n_dots=2)
    return qddevice

@pytest.fixture
def simulation_parameters():
    qddevice = QDDevice()
    qddevice.one_dimensional_dots_array(n_dots=2)
    
    return [
            {'device': qddevice, 
            'simulate': 'Electrons',
            'scanning_gate_indexes': [[0, 1], [1,2]],
            'n_points_per_axis': 60,
            'v_range_x': [-5, 20],
            'v_range_y': [-5, 20],
            'solver': 'MOSEK',
            'fix_voltage': 0,
            'gate_voltages': [[None, 1.5, None], [None, None, 1.5]],
            'voltage_ocupation_data': True,
            'sensing_data': True,
            'current_data': True,
            'save_to_json': True,
            }
    ]

