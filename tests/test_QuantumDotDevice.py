"""
Unit tests for the QuantumDotDevice module.
"""

import pytest
from qdsim import QDDevice
from matplotlib import pyplot 

@pytest.fixture
def qddevice(): # must be a function
    qddevice = QDDevice()
    qddevice.one_dimensional_dots_array(n_dots=2)
    return qddevice


class TestQDDevice:
    """
    Tesst for the QDDevice class.
    """

    def test_print_device_info(self, capfd, qddevice):
        """
        Test print_device_info method of the QDDevice class.
        """
        qddevice.print_device_info()
        out, err = capfd.readouterr()

        expected_out = "Device type: in-line array\nNumber of dots: 2\nNumber of gates: 2\nPhysical dot locations: [(0, 0), (1, 0)]\nDot-dot mutual capacitance matrix:\n[[0.12 0.08]\n [0.08 0.12]]\nDot-gate mutual capacitance matrix:\n[[0.12 0.00]\n [0.00 0.12]]\n"
        assert out == expected_out

    def test_save_to_file(self, qddevice):
        """
        Test save_to_file method save outputs to a file.
        """

        # TODO: Implement this test
        pass

    def test_one_dimensional_dots_array(self, qddevice):
        """
        Test one_dimensional_dots_array produces a one dimensional array of quantum dots.
        """
        qddevice.one_dimensional_dots_array(n_dots=2)
        assert qddevice._num_dots == 2
        assert qddevice._num_gates == 2
        assert isinstance(qddevice._physical_dot_locations, list)
        # check the length of the array matches the number of dots
        assert len(qddevice._physical_dot_locations) == qddevice._num_dots

    def test_plot_device(self, qddevice):
        """
        Test plot_device method produces a 'figure' of the device using matplotlib.
        We use the numbers of axes to check if the plot is produced, 
        but we do not check the content of the plot.
        """
        figure = qddevice.plot_device()
        
        # assert figure is not None, "Figure was not created."
        assert isinstance(figure, pyplot.Figure), "Created object is not a figure."
        assert len(figure.get_axes()) > 0, "Figure has no axes."
        
    # TODO: Implement more unit tests for the QDSimulator class

