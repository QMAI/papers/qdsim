"""
Unit tests for the QuantumDotSimulator module.
"""

import pytest
from qdsim import QDSimulator

@pytest.fixture
def qdsimulator_electrons():
    qdsimulator = QDSimulator('Electrons')
    return qdsimulator


class TestQDSimulator:
    """
    Test for the QDSimulator class.
    """

    def test_set_sensor_locations(self, qdsimulator_electrons):
        """
        Test set_sensor_locations method sets the sensor locations.
        """
        qdsimulator_electrons.set_sensor_locations(sensor_locations=[(0, 0), (1, 1)])
        assert qdsimulator_electrons._sensor_locations == [(0, 0), (1, 1)]

    def test_set_sensor_locations_error(self, qdsimulator_electrons):
        """
        Test set_sensor_locations method raises an error when sensor locations are not a list
        of tuples.
        """
        with pytest.raises(ValueError):
            qdsimulator_electrons.set_sensor_locations(sensor_locations=(0, 0))

    # TODO: Implement more unit tests for the QDSimulator class
    