# Set Up Environment


Describe what a developer needs to know to develop the tool further. For example:
 - How to set up a development environment (tools and procedures). REQUIREMENT: MOSEK
 - How to run test (pytest) -- (TODO)
 - How to pack and distribute new version of the software (if relevant).  
 - If the software is open to public collaboration. Explain how you prefer potential collaborators to  provide feeback or make contributions. See example: https://github.com/openearth/aeolis-python/blob/main/CONTRIBUTING.md 