# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'QDsim'
copyright = '2024, QDsim developers'
author = 'QDsim developers'
girlab_url = 'https://gitlab.com/QMAI/papers/qdsim'
release = '1.0.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "nbsphinx",
    "sphinx_copybutton",
    "myst_parser",
    "sphinx_rtd_theme",
]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_rtd_theme"
html_theme_options = {
    "repository_url": "https://gitlab.com/QMAI/papers/qdsim",
    "use_repository_button": True,
    "use_issues_button": True,
    "use_edit_page_button": True,
    "path_to_docs": "docs/source",
    "repository_branch": "main",
    "use_download_button": True,
    "home_page_in_toc": True,
    "logo": {
        "image_light": "_static/logo.svg", # TODO: change this
        "image_dark": "_static/logo_dark.svg", # TODO: change this
    }
}


html_static_path = ['_static']
