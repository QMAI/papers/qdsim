.. QDsim documentation master file, created by
   sphinx-quickstart on Mon Jul  8 12:50:43 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


QDsim's Documentation
=================================

.. toctree::
   :maxdepth: 2
   :caption: User documentation
   
   user/quickstart



.. toctree::
   :maxdepth: 2
   :caption: Tutorials

   tutorials/tutorialsmd/example1_double_quantum_dot
   tutorials/tutorialsmd/example2_4x4_crossbar_array
   tutorials/tutorialsmd/example3_completely_custom_device
   tutorials/tutorialsmd/example4_saving_and_loading_data
   tutorials/tutorialsmd/cluster

   

.. toctree::
   :maxdepth: 2
   :caption: Developer documentation

   developer/api-reference




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
