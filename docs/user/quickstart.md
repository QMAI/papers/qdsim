# Quick Start

QDsim stands for Quantum Dots simulator. It efficiently and quickly simulates charge stability diagrams for large quantum dot devices (even 100+ dots).
It was developed to make the simulation process as quick and as user-friendly as possible.
It is based on the constant interaction model, therefore it does not account for the quantum mechanical effects of the dots,
but its simplicity allows for quick and efficient creation of datasets.

Here are the minimum steps a user needs to do to install and use the tool. We show how to use the tool for the most common use case(s). Avoid explaining in details why something is done in a certain way or what the results mean. When more detail explanations might be necessary, add links to other parts of the doucmentation that contains the detials.


## Installation

To install QDsim, there are two options: using pip or cloning the repository. The first option is recommended for most users, while the second option is recommended for developers.

### Using pip
<pre>pip install qdsim </pre>

In order to use the package, it is further required to manually install either the MOSEK solver (licensed, free for academics) or the SCIP solver (open source).

### Cloning the repository

Clone the repository:  <pre>git clone https://gitlab.com/QMAI/papers/qdsim.git cd QDsim </pre>
Create a virtual environment (optional but recommended):  <pre> conda create -n myenv python=3.9 </pre>
Activate the virtual environment:  <pre>conda activate myenv </pre>
Install the dependencies:  <pre>pip install -r requirements.txt </pre>
Install QDsim:  <pre>pip install . </pre>

In order to use the package, it is further required to manually install either the MOSEK solver (licensed, free for academics) or the SCIP solver (open source).

You are now ready to use QDsim. For usage instructions, refer to the Usage section.

#### For developers
Instead of the last command, run:  <pre>pip install -e .[dev] </pre>



## Description

The package is composed of two main classes: `QDDevice` and `QDSimulator`.
The `QDDevice` class is used to create a quantum dot device, which can be either a pre-defined device or a custom device.
The `QDSimulator` class is used to set up the simulation environment and run the simulation.

The package is designed to be user-friendly and efficient, allowing for the simulation of charge stability diagrams for large quantum dot devices (even 100+ dots) in a matter of minutes.

In order to simulate the charge stability diagram of an (arbitrary) quantum dot device, just do the following:
1. Take advantage of a pre-defined device or create your own custom device via a  `QDDevice` object;
2. Set up the simulation environment (e.g. sensor locations, gates to be scanned, voltage ranges, etc.) via a `QDSimulator` object and run the simulation;
3. Save the data (and/or plots) and enjoy!

A practical example is shown below in the [Example](#Example) section.

All the features available are described in the tutorial folder, in which you can find 4 different Jupyter notebooks, each with a step-by-step guide on how to use the package.
Each notebooks focuses on a different aspect of the package, from the creation of a custom device to the simulation of a charge stability diagram, with all its possible options and features.
We suggest to read them in order, as they are designed to be read sequentially, from the first to the last.
They were designed to reduce redundancy of information and to provide a clear and concise guide on how to fully take advantage of the package.

### Example
Here is a quick example of how to use this project. Look at the Tutorials section for more examples and a step-by-step guide on how to use the package.
    
```python
from qdsim import QDDevice, QDSimulator

# Step 1: Create a quantum dot device
# use a pre-defined double dot device
qddevice = QDDevice() # Create a QDDevice object
qddevice.one_dimensional_dots_array(n_dots=2) # Create a 1D array of 2 quantum dots

# Step 2: Set up the simulator
qdsimulator = QDSimulator('Electrons')
# set the sensor location from which the charge stability diagram is measured
qdsimulator.set_sensor_locations([[2, 1]])
# Simulate the charge stability diagram
qdsimulator.simulate_charge_stability_diagram(
    qd_device=qddevice, v_range_x=[-5, 20], solver='MOSEK',
    v_range_y=[-5, 20], n_points_per_axis=60,
    scanning_gate_indexes=[0, 1])

# Step 3: Plot the charge stability diagram
qdsimulator.plot_charge_stability_diagrams()
```
![png](../../results/figures/DQD_current.png)

