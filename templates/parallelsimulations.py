"""
@author: Yasel Quintero & Manuel Garcia

Template for setting up and running a parallel QDSim simulation using QDParallelSimulator.

This script demonstrates how to assemble the 'setups' argument for the
QDParallelSimulator.parallel_simulate_charge_stability_diagram function.

The parallel function acts as a wrapper for the following functions:
    - QDSimulator class constructor
    - QDSimulator.simulate_charge_stability_diagram
    - QDSimulator.plot_charge_stability_diagrams

The user may want to look at the definition of these functions for more details on how to assemble
the list of simulation setups.
"""
import qdsim
from qdsim import QDDevice
from qdsim import QDParallelSimulator

# Create and customize a quantum dot device instance
device = QDDevice()

# Simulation setups (List of dictionaries)
# Each dictionary represents the setup for one or more simulations on a single device
# Th
# The dictionary for each setup must contain all the keys listed below
# Any number of setups are allowed (i.e. List can have any number of elements)
simsetups = [
    {
        'qd_device': device,                # QDDevice object
        'simulation_type': 'Electrons',     # string, 'Electrons' or 'Holes'
        'configurations': [
            # List of Tuples. Each Tuple represents the configuration for a single simulation of the charge stability diagram for the device.
            # Configurations are specified with a Tuple with the following elements:
            #   - sensor_location: tuple representing the x and y coordinates of the sensor; only 1 sensor per config
            #   - scanning_gate_indexes: tuple representing the indexes of the gates to scan; only 2 gates per config
            #   - gates_voltages: tuple containing the voltages to apply to each gate; can be set to None
            #   - fixed_voltage: float representing the voltage to apply to all the gates that are not being scanned; can be set to None
            ((2, 1), (0, 1), None, 1.5),
            ((1, 2), (2, 3), (1.89, 1.56, None, None), None),
        ],
        'n_points_per_axis': 60,            # int, number of points to probe on the x- and y-axis
        'v_range_x': [-5, 20],              # list, minimum and maximum voltage to probe on the x-axis
        'v_range_y': [-5, 20],              # list, minimum and maximum voltage to probe on the y-axis
        'solver': 'SCIP',                   # str, solver to use for the simulation ('SCIP' or 'MOSEK')
        'save_voltage_occupation_data': True,
        'save_sensing_data': True,
        'save_current_data': True,
        'save_charge_stability_diagram_plot': True,
        'charge_stability_diagram_plot_settings': {
            'cmapvalue': 'RdPu',            # str, default from matplotlib colormaps
            'gaussian_noise': False,        # bool, whether to add gaussian noise to the plot
            'white_noise': False,           # bool, whether to add white noise to the plot
            'pink_noise': False,            # bool, whether to add pink noise to the plot
            'plot_potential': False,        # bool, if False plot current data, if True plot potential data

            # Noise parameters
            'gaussian_noise_params': None,  # None or list of 2 elements [avg, std_dev]
            'white_noise_params': None,     # None or list of 2 elements [min, max]
            'pink_noise_params': None,      # None or list of 2 elements [f_max, amplitude_range]

            'plot_format_extension': 'png'  # str, file format for plot (e.g., 'png', 'pdf', 'jpeg')
        }
    }
]

# Run the parallel simulation for the charge stability diagram
QDParallelSimulator.parallel_simulate_charge_stability_diagram(simsetups, '')