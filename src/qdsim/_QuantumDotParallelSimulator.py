"""
@author: Yasel Quintero & Manuel Garcia

"""
from qdsim._QuantumDotDevice import QDDevice
from qdsim._QuantumDotSimulator import QDSimulator
from mpi4py import MPI
from typing import Tuple
from typing import List
import sys


class QDParallelSimulator:

    @staticmethod
    def parallel_simulate_charge_stability_diagram(setups: List[dict], filenameID: str = '') -> None:
        """
        Performs parallel simulations of charge stability diagrams using MPI.

        This function validates input parameters, prints the number of simulations if the process is the root,
        distributes simulation setups among MPI processes, runs the simulations, and saves the results.

        Args:
            setups (List[dict]): A list of dictionaries containing the setups for the simulations.
            filenameID (str, optional): A unique identifier for the output filenames. Defaults to ''.

        Returns:
            None
        """
        rank = MPI.COMM_WORLD.Get_rank()        # id of the MPI process executing this function
        comm_size = MPI.COMM_WORLD.Get_size()   # number of MPI processes

        if (rank == 0):
            QDParallelSimulator._validate_input_parameters(setups)
            QDParallelSimulator._print_number_of_simulations(setups)

        MPI.COMM_WORLD.barrier() # synchronize MPI processors

        sims = QDParallelSimulator._create_simulation_list(setups, filenameID, rank, comm_size)

        for sim in sims:
            # Create simulator object
            qdsimulator = QDSimulator() if (sim['simulation_type'] is None) else QDSimulator(sim['simulation_type'])
            # Assemble function args for 'simulate_charge_stability_diagram'
            if (sim['sensor_locations'] is not None):
                qdsimulator.set_sensor_locations(sim['sensor_locations'])
            # Run the simulation
            qdsimulator.simulate_charge_stability_diagram(**QDParallelSimulator._assemble_args_for_simulation_function(sim))
            # Save simulaton deatils
            qdsimulator.save_to_json(sim['output_filename_prefix'] + '_simulaton_config.json')
            # Save plot
            if (sim['save_charge_stability_diagram_plot'] is True):
                qdsimulator.plot_charge_stability_diagrams(**QDParallelSimulator._assemble_args_for_plot_function(sim))

    
    @staticmethod
    def _validate_input_parameters(setups: List[dict]) -> None:
        """Validate the simulation parameters for the parallel simulation. 
        Simulation parameters are organized in setups, each setup is a list of name:value pairs.

        Args:
            setups (List[dict]): A list of dictionaries representing the simulation setups.

        Raises:
            TypeError: If any simulation parameter does not match the expected data types.
            TypeError: If any configuration in the setups contain values that do not match the expected data types.
            ValueError: If any setup dictionary is missing required keys or contains invalid keys.
            ValueError: If any element in the a configuration does not have the expected length.
          
        Returns:
            None
        """

        valid_keys = ['qd_device', 'simulation_type', 'configurations', 'n_points_per_axis', 'v_range_x', 'v_range_y',
                'solver', 'save_voltage_occupation_data', 'save_sensing_data', 'save_current_data',
                'save_charge_stability_diagram_plot', 'charge_stability_diagram_plot_settings']

        plot_keys = ['cmapvalue', 'gaussian_noise', 'white_noise', 'pink_noise', 'plot_potential',
                     'gaussian_noise_params', 'white_noise_params', 'pink_noise_params', 'plot_format_extension']

        # check all elements in setups are dictionaries
        for setup in setups:
            if not isinstance(setup, dict):
                raise TypeError(f"Setup for 'qd_device:'{setup['qd_device']} is not a dictionary.")
        
        # check for invalid keys 
        for i in range(0, len(setups)):
            invalid_keys = [key for key in setups[i] if key not in valid_keys]
            if invalid_keys:
                raise ValueError(f"Setup {i} contains the following invalid parameters: {invalid_keys}")
        
        # check missing keys
        for i in range(0, len(setups)):
            missing_keys = [key for key in valid_keys if key not in setups[i]]
            if missing_keys:
                raise ValueError(f"Setup {i} is missing the following parameters: {missing_keys}")
        
        # check configurations
        for i in range(0, len(setups)):
            conf = setups[i]['configurations'] 
            if not isinstance(conf, list):
                raise TypeError(f"Configurations in setup {i} is not a list.")
            for j in range(0, len(conf)):
                if not isinstance(conf[j], tuple):
                    raise TypeError(f"Configurations Error: item {j} in setup {i} is not a tuple.")
                if (len(conf[j]) != 4):
                    raise ValueError(f"Configurations Error: item {j} in setup {i} expects 4 elements. Found {len(conf[j])}")
            
                for k in range(0, len(conf[j])):
                    # check sensor location
                    if k == 0:
                        if conf[j][k] is None:
                            continue
                        elif not isinstance(conf[j][k], Tuple):
                            raise TypeError(f"Configurations Error: item {j} in setup {i} expects a tuple or None for sensor location.")
                        elif len(conf[j][k]) != 2:
                            raise ValueError(f"Configurations Error: item {j} in setup {i} expects a tuple of length 2 for sensor location.")
                        elif not all(isinstance(x, (int, float)) for x in conf[j][k]):
                            raise TypeError(f"Configurations Error: item {j} in setup {i} expects a tuple of floats or ints for sensor location.")
                        
                    # check scanning gate indexes
                    if k == 1:
                        if conf[j][k] is None:
                            continue
                        elif not isinstance(conf[j][k], Tuple):
                            raise TypeError(f"Configurations Error: item {j} in setup {i} expects a tuple or None for scanning gate indexes.")
                        elif len(conf[j][0]) != 2:
                            raise ValueError(f"Configurations Error: item {j} in setup {i} expects a tuple of length 2 for scanning gate indexes.")
                        elif not all(isinstance(x, int) and x >= 0 for x in conf[j][k]):
                            print('value ', conf[j][k])
                            raise TypeError(f"Configurations Error: item {j} in setup {i} expects a tuple  positive intergers for scanning gate indexes.")
                        
                    # check gate voltages
                    if k == 2:
                        if conf[j][k] is None:
                            continue
                        elif not isinstance(conf[j][k], Tuple):
                            raise TypeError(f"Configurations Error: item {j} in setup {i} expects a tuple or None gate voltages.")
                        elif not all(isinstance(x, (int, float)) or x is None for x in conf[j][k]):
                            print('value ', conf[j][k])
                            raise TypeError(f"Configurations Error: item {j} in setup {i} expects a tuple of intergers or floats for gate voltages.")
                    
                    # check fixed voltage
                    if k == 3:
                        if conf[j][k] is None:
                            continue
                        elif not isinstance(conf[j][k], (int, float)) and conf[j][k] is not None:
                            raise TypeError(f"Configurations Error: item {j} in setup {i} expects a float or integer for fixed voltage.")

        # check plot settings
        for i in range(0, len(setups)):
            plotsettings = setups[i]['charge_stability_diagram_plot_settings']
            invalid_keys = [key for key in plotsettings if key not in plot_keys]
            if invalid_keys:
                raise ValueError(f"Setup {i} contains the following invalid parameters: {invalid_keys}")
            missing_keys = [key for key in plot_keys if key not in plotsettings]
            if missing_keys:
                raise ValueError(f"Setup {i} is missing the following parameters: {missing_keys}")

        return None


    @staticmethod
    def _create_simulation_list(setups: List[dict], filenameID: str, rank: int, comm_size: int) -> List[dict]:
        """
        Creates a list of simulation setups and distributes them among MPI processes.

        Each simulation setup is created based on the provided configurations and assigned a unique output filename prefix.
        The list is then distributed such that each MPI process gets a subset of the list.

        Args:
            setups (List[dict]): A list of dictionaries, each containing simulation setup information and configurations.
            filenameID (str): A unique identifier for the output filenames.
            rank (int): The rank of the MPI process.
            comm_size (int): The total number of MPI processes.

        Returns:
            List[dict]: The subset of the simulation list assigned to the MPI process.
        """
        # Create a list of _SimulationSetup
        simulation_list = []
        for i in range(0, len(setups)):
            for j in range(0, len(setups[i]['configurations'])):
                # Copy all elements in the dictionary except for 'configurations'
                simulation_list.append(QDParallelSimulator._dict_without_keys(setups[i], ['configurations']))
                # Add the config keys to the dictionary in the last element of simulation_list
                simulation_list[-1]['sensor_locations'] = [setups[i]['configurations'][j][0]] # list of tuples
                simulation_list[-1]['scanning_gate_indexes'] = list(setups[i]['configurations'][j][1]) # list of 2 ints
                simulation_list[-1]['gates_voltages'] = list(setups[i]['configurations'][j][2])
                simulation_list[-1]['fixed_voltage'] = setups[i]['configurations'][j][3]
                simulation_list[-1]['output_filename_prefix'] = 'setup' + str(i) + '_config' + str(j) + '_' + filenameID

        # Each MPI process gets a subset of the list
        return QDParallelSimulator._get_list_subset(simulation_list, rank, comm_size)


    @staticmethod
    def _get_list_subset(simulation_list: List[dict], rank: int, comm_size: int) -> List[dict]:
        """
        Distributes a list of simulations among MPI processes.

        If there are more simulations than MPI processes, each process gets a subset of the list.
        If there are more MPI processes than simulations, only processes with rank less than the number of simulations get a simulation.

        Args:
            simulation_list (List[dict]): The list of simulations to distribute.
            rank (int): The rank of the MPI process.
            comm_size (int): The total number of MPI processes.

        Returns:
            List[dict]: The subset of the simulation list assigned to the MPI process.
        """
        # If we have more simulations than MPI processes
        if (comm_size <= len(simulation_list)):
            base_size = len(simulation_list) // comm_size
            start = rank * base_size
            end = start + base_size - 1
            # Each MPI process gets a subset of the list 
            subset = simulation_list[start:end+1]
            # Assign leftover elements
            if(rank < len(simulation_list) % comm_size):
                i = -1 - rank
                subset.append(simulation_list[i])
            return subset
        else:
            # if we have more MPI processes than simulations
            if (rank < len(simulation_list)):
                return simulation_list[rank]
            else:
                return []


    @staticmethod
    def _dict_without_keys(d: dict, keys: List) -> dict:
        """
        Returns a dictionary excluding specified keys.

        Args:
            d (dict): The original dictionary.
            keys (List): A list of keys to exclude from the original dictionary.

        Returns:
            dict: A new dictionary with the specified keys excluded.

        Example:
            >>> original_dict = {'a': 1, 'b': 2, 'c': 3}
            >>> keys_to_exclude = ['b', 'c']
            >>> _dict_without_keys(original_dict, keys_to_exclude)
            {'a': 1}
        """
        return {x: d[x] for x in d if x not in keys}


    @staticmethod
    def _assemble_args_for_simulation_function(setup: dict) -> dict:
        """Assembles the arguments for the QDSimulator.simulate_charge_stability_diagram function into a dictionary

        Args:
            setup (dict): A dictionary representing the setup for a single simulation

        Returns:
            dict: A dictionary containing all the arguments for the simulation function.

        Notes:
            The names of the keys in the return dictionary exactly match the names of the arguments in the simulation function
        """
        keys = ['simulation_type', 'sensor_locations', 'save_voltage_occupation_data',
                'save_sensing_data', 'save_current_data', 'output_filename_prefix',
                'save_charge_stability_diagram_plot', 'charge_stability_diagram_plot_settings']
        if (setup['v_range_y'] is None):
            keys.append('v_range_y')
        if (setup['solver'] is None):
            keys.append('solver')
        if (setup['fixed_voltage'] is None):
            keys.append('fixed_voltage')
        if (setup['gates_voltages'] is None):
            keys.append('gates_voltages')
        args = QDParallelSimulator._dict_without_keys(setup, keys)
        args['use_ray'] = False

        # Add saving arguments to dictionary
        prefix = setup['output_filename_prefix']
        if (setup['save_voltage_occupation_data'] is True):
                args['save_occupation_data_to_filepath'] = prefix + '_voltage_occupation.npy'
        if (setup['save_sensing_data'] is True):
                args['save_sensing_data_to_filepath'] = prefix + '_sensing.npy'
        if (setup['save_current_data'] is True):
                args['save_current_data_to_filepath'] = prefix + '_current.npy'

        return args
 

    @staticmethod
    def _assemble_args_for_plot_function(setup: dict) -> dict:
        """Assembles the arguments for the QDSimulator.plot_charge_stability_diagrams function into a dictionary

        Args:
            setup (dict): A dictionary representing the setup for a single simulation

        Returns:
            dict: A dictionary containing all the arguments for the plot function.

        Notes:
            The names of the keys in the return dictionary exactly match the names of the arguments in the plot function
        """
        # Create a new dictionary
        args = QDParallelSimulator._dict_without_keys(setup['charge_stability_diagram_plot_settings'], 'plot_format_extension')
        # Add saving arguments to dictionary
        prefix = setup['output_filename_prefix']
        args['save_plot_to_filepath'] = prefix + '_charge_stability_diagram.' + setup['charge_stability_diagram_plot_settings']['plot_format_extension']
        args['save_noisy_npy_data_to_filepath'] = prefix + '_noisy_plotdata.npy'
        return args


    @staticmethod
    def _print_number_of_simulations(setups: List[dict]) -> None:
        """Prints the total number of simulations to be performed according to the setup parameters.

        Args:
            setups (List[dict]): A list of dictionaries representing the simulation setups.

        Returns:
            str: A message indicating the total number of simulations to be performed.

        """
        n_simulations = 0
        for setup in setups:
            n_simulations += len(setup['configurations'])
        message = f"QDSIM: \n Number of simulations to run: {n_simulations} \n"
        sys.stdout.write(message)

